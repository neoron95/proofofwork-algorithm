﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ProofOfWork
{
    class Program
    {
        public static int CurrentTime = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

        static void Main()
        {
            for (int difficulty = 2; difficulty < 6; difficulty++)
            {
                int numberOfSolutionsFound = 0;
                int finishTime = CurrentTime + 15; // Выбираем промежуток времени за который будем искать (15 секунд)
                do
                {
                    ChallengeResult result = SolveTheChallenge(RandomStringGenerator.GenerateByLen(10), difficulty);
                    numberOfSolutionsFound++;
                    CurrentTime = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                } while (CurrentTime < finishTime);

                Console.WriteLine("Total number of solutions found for (difficulty = " + difficulty + " ): {" + numberOfSolutionsFound + "}");
            }
            Console.ReadKey();
        }

        private static ChallengeResult SolveTheChallenge(string challengeString, int difficulty)
        {
            var challengeResult = new ChallengeResult();
            string expectedResult = new string('0', difficulty);  // если сложность = 4 то expectedResult будет = '0000'
            var stopwatch = new Stopwatch();
            stopwatch.Start(); // Используем для подсчета потраченого времени
            do
            {
                challengeResult.NumberOfIterations++; // Считаем количество итераций
                challengeResult.SolveString = challengeString + RandomStringGenerator.GenerateByLen(10); // генерация случайной строки
                challengeResult.SolveHash = Sha256Generator.GenerateSha256String(challengeResult.SolveString);  // Генерация sha-256 из случайной строки
            } while (!challengeResult.SolveHash.StartsWith(expectedResult)); // проверем нашли ли мы решение
            challengeResult.SolveSeconds = stopwatch.Elapsed.TotalSeconds;
            return challengeResult;
        }
    }

    public class RandomStringGenerator
    {
        private static readonly Random Random = new Random();
        public static string GenerateByLen(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }
    }

    public class Sha256Generator
    {
        public static string GenerateSha256String(string inputString)
        {
            var sha256 = SHA256.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            foreach (byte t in hash)
                result.Append(t.ToString("X2"));
            return result.ToString();
        }
    }

    public class ChallengeResult
    {
        public string SolveString { get; set; }
        public double SolveSeconds { get; set; }
        public string SolveHash { get; set; }
        public int NumberOfIterations { get; set; }
    }
}
